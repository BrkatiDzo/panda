import { Injectable } from '@angular/core';
import { PandaService } from '../services/pande.services';


@Injectable()
export class PandaModel{

    pande=[];

    constructor(private service:PandaService){
        let posmatracSvihPandi=this.service.vratiPosmatracSvihPandaNaServeru();
        posmatracSvihPandi.subscribe(
            (pande)=>{
                this.pande=pande;
            }
        );
    }
    
}