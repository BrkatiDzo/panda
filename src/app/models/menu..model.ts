import { Injectable } from '@angular/core';
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class MenuModel {

  constructor(public router:Router){}

  linkovi=[
    {putanja:'/', naziv:'Kuca'},
    {putanja:'/dashboard', naziv:'Tabela Panda'},
    {putanja:'/pande/nova', naziv:'Dodaj Pandu'},
  ]

  goToPath(path){
    let pathArray = [];
    pathArray.push(path);
    this.router.navigate(pathArray);
  }
  
}
