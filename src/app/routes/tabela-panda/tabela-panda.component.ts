import { Component, OnInit } from '@angular/core';
import { PandaModel } from '../../models/panda.model';

@Component({
  selector: 'tabela-panda',
  templateUrl: './tabela-panda.component.html',

})
export class TabelaPandaComponent implements OnInit {

  constructor(public model: PandaModel) { }

  ngOnInit() {
  }

}
