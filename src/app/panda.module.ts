import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http'

import { PandaComponent } from './panda.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';

import { MenuModel } from './models/menu..model';


import { HomeComponent } from './routes/home/home.component';
import { TabelaPandaComponent } from './routes/tabela-panda/tabela-panda.component';
import { NovaPandaComponent } from './routes/nova-panda/nova-panda.component';
import { EditujPanduComponent } from './routes/edituj-pandu/edituj-pandu.component';
import { DetaljiPandeComponent } from './routes/detalji-pande/detalji-pande.component';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { PandaModel } from './models/panda.model';
import { PandaService } from './services/pande.services';


const routes:Routes = [
  {path:'',component:HomeComponent},
  {path:'dashboard',component:TabelaPandaComponent},
  {path:'pande/nova',component:NovaPandaComponent},
  {path:'panda',component:PandaComponent},
  {path:'panda/:id/edit',component:EditujPanduComponent},
  {path:'panda/:id/info',component:DetaljiPandeComponent}
 ]

@NgModule({
  declarations: [
    PandaComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TabelaPandaComponent,
    NovaPandaComponent,
    EditujPanduComponent,
    DetaljiPandeComponent,
    MenuLinkComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule
  ],
  providers: [
    MenuModel,
    PandaModel,
    PandaService
  ],
  bootstrap: [PandaComponent]
})
export class PandaModule { }
