import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { config } from '../config/config';
import 'rxjs/add/operator/map';

@Injectable()
export class PandaService{

    constructor(private http:Http){}

    vratiPosmatracSvihPandaNaServeru(){
        return this.http
        .get(config.apiUrl + '/pande')
        .map((response) => {return response.json() });
    }

}